package com.sicredi.airBNB.entities;

public enum PropertyType {
    FLAT, ROOM, HOUSE
}
