package com.sicredi.airBNB.entities;

import com.sicredi.airBNB.DTO.PropertyDTO;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("properties")
public class Property {
    @Id
    private ObjectId id;
    private String name;
    private PropertyStatus propertyStatus;
    private PropertyType propertyType;
    private City city;
    private Double price;

    public Property() {
    }

    public Property(PropertyDTO dto){
        this(dto.getName(), dto.getPropertyStatus(), dto.getPropertyType(), dto.getCity(), dto.getPrice());
    }

    public Property(String name, PropertyStatus propertyStatus, PropertyType propertyType, City city, Double price) {
        this.name = name;
        this.propertyStatus = propertyStatus;
        this.propertyType = propertyType;
        this.city = city;
        this.price = price;
    }

    public ObjectId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PropertyStatus getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(PropertyStatus propertyStatus) {
        this.propertyStatus = propertyStatus;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }
}
