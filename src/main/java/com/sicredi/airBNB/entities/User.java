package com.sicredi.airBNB.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.HashSet;
import java.util.Set;

@Document(collection = "user")

public class User {
    @Id
    private ObjectId id;
    private String user;
    private String password;
    private Boolean status = true;
    private UserType userType;
    @DocumentReference
    private Set<Property> propertySet = new HashSet<>();
    
    public User() {
    }

    public User(String user,String password, Boolean status, UserType userType) {
        this.user = user;
        this.password = password;
        this.status = status;
        this.userType = userType;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Set<Property> getPropertySet() {
        return propertySet;
    }

    public void addPropertySet(Set<Property> propertySet) {
        this.propertySet.addAll(propertySet) ;
    }

    public void addProperty(Property propertySet) {
        this.propertySet.add(propertySet) ;
    }

    public void removeProperty(Property property){
        this.propertySet.remove(property);
    }
}
