package com.sicredi.airBNB.entities;

public enum UserType {

    CLIENT, LANDLORD;
    
}
