package com.sicredi.airBNB.services;

import org.springframework.stereotype.Service;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import com.sicredi.airBNB.services.exceptions.EntityNotFoundException;

import com.sicredi.airBNB.DTO.UserDTO;
import com.sicredi.airBNB.entities.User;
import com.sicredi.airBNB.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private final String errorMessage = "Entity could not be found";

    @Autowired 
    UserRepository repository;

    @Transactional(readOnly = true)
    public Page<UserDTO> findAllPaged(Pageable pageable){
        Page<User> list = repository.findAll(pageable);
        return list.map(x-> new UserDTO(x));
    }

    @Transactional(readOnly = true)
    public UserDTO findById (String id) {
        Optional <User> obj = repository.findById(id);
        User entity = obj.orElseThrow(()-> new EntityNotFoundException(errorMessage));
        return new UserDTO(entity);
    }

    @Transactional
    public UserDTO insert(UserDTO dto) {
        User entity = new User();
        validatingUser(dto.getUser());
        copyDtoToEntity(dto, entity);
        entity = repository.save(entity);
        return new UserDTO(entity);
    }

    @Transactional
    public UserDTO update(String id, UserDTO dto) {
        Optional<User> optional = repository.findById(id);
        User entity = optional.orElseThrow(()-> new EntityNotFoundException(errorMessage));
        copyDtoToEntity(dto, entity);
        entity = repository.save(entity);
        return new UserDTO(entity);
    }

    public void copyDtoToEntity(UserDTO dto, User entity){
        entity.setUser(dto.getUser());
        entity.setPassword(dto.getPassword());
        entity.setStatus(dto.getStatus());
        entity.setUserType(dto.getUserType());
    } 

    public Boolean validatingUser(String user){
        repository.findByUser(user)
            .ifPresent(x -> {
                throw new RuntimeException("Usuário já cadastrado");
            });
        return true;
    }
}
