package com.sicredi.airBNB.services;


import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.sicredi.airBNB.DTO.UserDTO;

public interface UserService {

    Page<UserDTO> findAllPaged(Pageable pageable);

    UserDTO findById(String id);

    UserDTO insert (UserDTO dto);

    UserDTO update (String id, UserDTO dto);
}
