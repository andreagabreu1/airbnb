package com.sicredi.airBNB.services;

import com.sicredi.airBNB.DTO.PropertyDTO;
import com.sicredi.airBNB.entities.PropertyStatus;
import com.sicredi.airBNB.entities.Property;
import com.sicredi.airBNB.entities.User;
import com.sicredi.airBNB.entities.UserType;
import com.sicredi.airBNB.repositories.PropertyRepository;
import com.sicredi.airBNB.repositories.UserRepository;
import com.sicredi.airBNB.services.exceptions.EntityNotFoundException;
import com.sicredi.airBNB.services.exceptions.ForbiddenUserException;
import com.sicredi.airBNB.services.exceptions.UnavaiablePropertyException;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PropertyService {
    private final String errorMessage = "Entity could not be found";
    private PropertyRepository propertyRepository;
    private UserRepository userRepository;

    @Autowired
    public PropertyService(PropertyRepository propertyRepository, UserRepository userRepository){
        this.propertyRepository = propertyRepository;
        this.userRepository = userRepository;
    }

    public Page<PropertyDTO> getPaginated(PageRequest pageRequest) {
        Page<Property> pagedList = this.propertyRepository.findAll(pageRequest);
        return pagedList.map(item -> new PropertyDTO(item));
    }

    public PropertyDTO getById(String id) {
        var amoma = new ObjectId(id);
        Optional<Property> optional = this.propertyRepository.findById(new ObjectId(id));
        Property entity = optional.orElseThrow(()-> new EntityNotFoundException(errorMessage));
        return new PropertyDTO(entity);
    }

    public PropertyDTO create(PropertyDTO body) {
        Property entity = new Property(body);
        this.propertyRepository.insert(entity);
        return new PropertyDTO(entity);
    }

    @Transactional
    public PropertyDTO update(String id, PropertyDTO body) {
        Optional<Property> optional = this.propertyRepository.findById(new ObjectId(id));
        Property entity = optional.orElseThrow(()-> new EntityNotFoundException(errorMessage));
        BeanUtils.copyProperties(body, entity);
        this.propertyRepository.save(entity);
        return new PropertyDTO(entity);
    }

    public void delete(String id) throws EntityNotFoundException{
        Optional<Property> optional = this.propertyRepository.findById(new ObjectId(id));
        Property entity = optional.orElseThrow(()-> new EntityNotFoundException(errorMessage));
        this.propertyRepository.delete(entity);
    }

    @Transactional
    public void leaseProperty(String user_id, String property_id) {
        User userEntity = this.userRepository.findById(user_id).orElseThrow(()-> new EntityNotFoundException("User not found"));
        Property propertyEntity = this.propertyRepository.findById(new ObjectId(property_id)).orElseThrow(()-> new EntityNotFoundException("Property not found"));

        if(userEntity.getUserType() != UserType.CLIENT){
            throw new ForbiddenUserException("User is not of required type:" + UserType.CLIENT);
        }
        if(propertyEntity.getPropertyStatus() == PropertyStatus.UNAVAIABLE){
            throw new UnavaiablePropertyException("Property not avaiable at this moment");
        }

        userEntity.addProperty(propertyEntity);
        this.userRepository.save(userEntity);

        propertyEntity.setPropertyStatus(PropertyStatus.UNAVAIABLE);
        this.propertyRepository.save(propertyEntity);
    }

    @Transactional
    public void checkout(String user_id, String property_id) {
        User userEntity = this.userRepository.findById(user_id).orElseThrow(()-> new EntityNotFoundException("User not found"));
        Property propertyEntity = this.propertyRepository.findById(new ObjectId(property_id)).orElseThrow(()-> new EntityNotFoundException("Property not found"));

        if(userEntity.getUserType() != UserType.CLIENT){
            throw new ForbiddenUserException("User is not of required type:" + UserType.CLIENT);
        }
        if(propertyEntity.getPropertyStatus() != PropertyStatus.UNAVAIABLE){
            throw new UnavaiablePropertyException("Property is avaiable already");
        }

        userEntity.removeProperty(propertyEntity);
        this.userRepository.save(userEntity);

        propertyEntity.setPropertyStatus(PropertyStatus.LEASE);
        this.propertyRepository.save(propertyEntity);
    }
}
