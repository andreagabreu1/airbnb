package com.sicredi.airBNB.services.exceptions;

public class ForbiddenUserException extends RuntimeException{
    public ForbiddenUserException(String message) {
        super(message);
    }
}
