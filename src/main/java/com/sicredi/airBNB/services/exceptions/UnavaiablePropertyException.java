package com.sicredi.airBNB.services.exceptions;

public class UnavaiablePropertyException extends RuntimeException{
    public UnavaiablePropertyException(String message) {
        super(message);
    }
}
