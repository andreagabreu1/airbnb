package com.sicredi.airBNB.repositories;

import com.sicredi.airBNB.entities.Property;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends MongoRepository<Property, ObjectId> {
}
