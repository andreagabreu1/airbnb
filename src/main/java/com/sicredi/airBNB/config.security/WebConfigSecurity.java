
package com.sicredi.airBNB.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebConfigSecurity {

    @Bean
    public InMemoryUserDetailsManager userDetailsManager() {
        UserDetails userAdmin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("admin")
                .roles("LANDLORD")
                .build();
        UserDetails userClient = User.withDefaultPasswordEncoder()
                .username("client")
                .password("client")
                .roles("CLIENT")
                .build();

        return new InMemoryUserDetailsManager(userAdmin, userClient);


    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/users/**").hasRole("CLIENT")
                .antMatchers(HttpMethod.GET, "/users/**").hasRole("LANDLORD")
                .antMatchers(HttpMethod.POST, "/users/**").hasRole("LANDLORD")
                .anyRequest().authenticated()
                .and()
                .csrf().disable();

        return http.build();
    }
}
