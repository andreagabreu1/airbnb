package com.sicredi.airBNB.DTO;

import org.bson.types.ObjectId;

import com.sicredi.airBNB.entities.User;
import com.sicredi.airBNB.entities.UserType;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDTO {
    private String id;
    private String user;
    private String password;
    private Boolean status = true;
    private UserType userType;
    private Set<PropertyDTO> propertySet = new HashSet<>();
    public UserDTO() {
    }

    public UserDTO(ObjectId id, String user, String password, Boolean status, UserType userType, Set<PropertyDTO> propertySet) {
        this.id = id.toString();
        this.user = user;
        this.password = password;
        this.status = status;
        this.userType = userType;
        this.propertySet = propertySet;
    }

    public UserDTO(User entity) {
        this.id = entity.getId().toString();
        this.user = entity.getUser();
        this.password = entity.getPassword();
        this.status = entity.getStatus();
        this.userType = entity.getUserType();
        this.propertySet = entity.getPropertySet().stream().map(item -> new PropertyDTO(item)).collect(Collectors.toSet());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Set<PropertyDTO> getPropertySet() {
        return propertySet;
    }

    public void setPropertySet(Set<PropertyDTO> propertySet) {
        this.propertySet = propertySet;
    }
}
