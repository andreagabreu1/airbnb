package com.sicredi.airBNB.DTO;

import com.sicredi.airBNB.entities.City;
import com.sicredi.airBNB.entities.PropertyStatus;
import com.sicredi.airBNB.entities.Property;
import com.sicredi.airBNB.entities.PropertyType;

public class PropertyDTO {
    private String id;
    private String name;
    private PropertyStatus propertyStatus;
    private PropertyType propertyType;
    private City city;
    private Double price;

    public PropertyDTO() {
    }

    public PropertyDTO(Property entity) {
        this(
            entity.getId().toString(),
            entity.getName(),
            entity.getPropertyStatus(),
            entity.getPropertyType(),
            entity.getCity(),
            entity.getPrice()
        );
    }

    public PropertyDTO(String id, String name, PropertyStatus propertyStatus, PropertyType propertyType, City city, Double price) {
        this.id = id;
        this.name = name;
        this.propertyStatus = propertyStatus;
        this.propertyType = propertyType;
        this.city = city;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PropertyStatus getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(PropertyStatus propertyStatus) {
        this.propertyStatus = propertyStatus;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }
}
