package com.sicredi.airBNB.controllers;

import com.sicredi.airBNB.DTO.PropertyDTO;
import com.sicredi.airBNB.services.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/properties")
public class PropertyController {
    private PropertyService service;

    @Autowired
    public PropertyController(PropertyService service){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<PropertyDTO>> getPaginated(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "12") Integer linesPerPage,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction,
            @RequestParam(value = "orderBy", defaultValue = "moment") String orderBy
    ){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        Page<PropertyDTO> list = this.service.getPaginated(pageRequest);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PropertyDTO> getById(@PathVariable String id){
        PropertyDTO response = this.service.getById(id);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping
    public ResponseEntity<PropertyDTO> create(@RequestBody PropertyDTO body){
        PropertyDTO response = this.service.create(body);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(body.getId()).toUri();
        return ResponseEntity.created(uri).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PropertyDTO> update(@PathVariable String id, @RequestBody PropertyDTO body){
        PropertyDTO response = this.service.update(id, body);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<PropertyDTO> delete(@PathVariable String id){
        this.service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/lease/{user_id}/{property_id}")
    public ResponseEntity<PropertyDTO> leaseProperty(@PathVariable String user_id, @PathVariable String property_id){
        this.service.leaseProperty(user_id, property_id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/checkout/{user_id}/{property_id}")
    public ResponseEntity<PropertyDTO> checkoutPropperty(@PathVariable String user_id, @PathVariable String property_id){
        this.service.checkout(user_id, property_id);
        return ResponseEntity.noContent().build();
    }
}
