package com.sicredi.airBNB.controllers.exceptions;

import com.sicredi.airBNB.services.exceptions.EntityNotFoundException;
import com.sicredi.airBNB.services.exceptions.ForbiddenUserException;
import com.sicredi.airBNB.services.exceptions.UnavaiablePropertyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionStandardBody> entityNotFoundException(EntityNotFoundException e, HttpServletRequest request){
        ExceptionStandardBody err = new ExceptionStandardBody(
                Instant.now(),
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                "Entity not found",
                request.getRequestURI()
        );
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(ForbiddenUserException.class)
    public ResponseEntity<ExceptionStandardBody> forbiddenUserExceptionHandler(ForbiddenUserException e, HttpServletRequest request){
        ExceptionStandardBody err = new ExceptionStandardBody(
                Instant.now(),
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                "Forbidden user type",
                request.getRequestURI()
        );
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
    }

    @ExceptionHandler(UnavaiablePropertyException.class)
    public ResponseEntity<ExceptionStandardBody> unavaiablePropertyExceptionHandler(UnavaiablePropertyException e, HttpServletRequest request){
        ExceptionStandardBody err = new ExceptionStandardBody(
                Instant.now(),
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                "Property unavaiable",
                request.getRequestURI()
        );
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
    }
}
